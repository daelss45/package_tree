#!/usr/bin/env ruby
require "socket"
require "thread"
require_relative "lib/process_request"

PORT = 8080
request_instance = Request.new
mutex = Mutex.new

Socket.tcp_server_loop(PORT) do |s|
  Thread.start do
    begin
      while message = s.gets
        mutex.synchronize do
          s.print request_instance.get_command_package_dependencies(message)
        end
      end
    end
  end
end
