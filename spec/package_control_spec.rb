require 'package_control'

describe PackageControl do
  let(:pkg_control) { PackageControl.new }

  describe "#index" do
    it "returns true if package can be indexed" do
      expect(pkg_control.index("pkg", [])).to eq(true)
    end
  end
  describe 'PackageControl' do
    describe 'attributes' do
      it "allows process control for: index" do
        pkg_control = PackageControl.new
        pkg_control.index("cloog", ["gmp","isl","pkg-config"])
        expect(pkg_control.index("cloog", ["gmp","isl","pkg-config"])).to eq(true)
      end
      it "allows process control for: remove" do
        pkg_control = PackageControl.new
        pkg_control.remove("cloogx")
        expect(pkg_control.remove("cloog")).to eq(true)
      end
      it "allows process control for: query" do
        pkg_control = PackageControl.new
        pkg_control.query("cloog")
        expect(pkg_control.query("cloog")).to eq(false)
      end
    end
  end
end
