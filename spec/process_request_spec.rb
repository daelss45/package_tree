require_relative "../lib/process_request"

describe Request do
  let(:proces_request) { Request.new }

  describe '#get_command_package_dependencies' do
    it "returns ERROR when command is invalid" do
      expect(proces_request.get_command_package_dependencies(nil)).to eq "ERROR\n"
    end

    it "indexes package when command is: INDEX" do
      expect_any_instance_of(PackageControl).to receive(:index).with("ceylon", [])
      proces_request.get_command_package_dependencies("INDEX|ceylon|\n")
    end

    it "removes package when command is: REMOVE" do
      expect_any_instance_of(PackageControl).to receive(:remove).with("ceylon", [])
      proces_request.get_command_package_dependencies("REMOVE|ceylon|\n")
    end

    it "queries package when command is: QUERY" do
      expect_any_instance_of(PackageControl).to receive(:query).with("ceylon", [])
      proces_request.get_command_package_dependencies("QUERY|ceylon|\n")
    end

    it "return ERROR package when command anything else" do
      expect(proces_request.get_command_package_dependencies("ANYTHING|ceylon|\n")).to eq("ERROR\n")
    end
  end

  describe "#message" do
    it "returns OK when the result is true" do
      expect(proces_request.message(true)).to eq("OK\n")
    end

    it "returns FAIL when the result is false" do
      expect(proces_request.message(false)).to eq("FAIL\n")
    end
  end

  describe "#valid_request" do
    it "returns true if input is valid" do
      expect(proces_request.valid_request?("QUERY|ceylon|\n")).to eq(true)
    end

    it "returns false if input does not start with: INDEX or REMOVE or QUERY" do
      expect(proces_request.valid_request?("ANYTHING|ceylon|\n")).to eq(false)
    end

    it "returns false if input has more than 2 pipes (|)" do
      expect(proces_request.valid_request?("INDEX|ce|yl|on|\n")).to eq(false)
    end

    it "returns false if input has less than 2 pipes (|)" do
      expect(proces_request.valid_request?("QUERY|ceylon\n")).to eq(false)
    end

    it "returns false if input does not end with '\n'" do
      expect(proces_request.valid_request?("QUERY|ceylon")).to eq(false)
    end
  end
end
