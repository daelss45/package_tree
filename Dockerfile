FROM ubuntu:latest

RUN \
  apt-get update && \
  apt-get install -y ruby ruby-dev ruby-bundler && \
  rm -rf /var/lib/apt/list/*

WORKDIR /package_tree

COPY . /package_tree

EXPOSE 8080
