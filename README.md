# package-tree: DigitalOcean's Coding Challenge

This is an implementation of a "package tree" written in Ruby. The server will open a TCP socket on port 8080, and  accepts connections from multiple clients at the same time, all trying to add and remove items to the index or query item concurrently. It has been tested on a Mac (./do-package-tree_darwin) and Ubuntu ./do-package-tree_linux), and with additional test suite written in Golang.

## Setup
Navigat to the directory containing the source code of the project.
In the project directory, `init.rb` is the entry point of the script to run. Below is the layout of the directory with all of its files.
```sh
$ tree
.
├── Dockerfile
├── README.md
├── init.rb
├── lib
│   ├── package_control.rb
│   └── process_request.rb
├── spec
│   ├── package_control_spec.rb
│   ├── process_request_spec.rb
│   └── spec_helper.rb
└── test-capture
    └── test-screen.txt
3 directories, 4 files

$ ls -l
total 40
-rw-r--r--  1 daelss45  staff   188 Oct  2 14:07 Dockerfile
-rw-r--r--  1 daelss45  staff    90 Oct  2 14:44 Gemfile
-rw-r--r--  1 daelss45  staff   534 Oct  2 14:44 Gemfile.lock
-rw-r--r--  1 daelss45  staff  1622 Oct  2 22:30 README.md
-rwxr-xr-x  1 daelss45  staff   395 Oct  2 09:15 init.rb
drwxr-xr-x  4 daelss45  staff   128 Oct  2 09:17 lib
drwxr-xr-x  5 daelss45  staff   160 Oct  2 10:29 spec
drwxr-xr-x  3 daelss45  staff    96 Oct  2 00:39 test-capture
Daels-MacBook-Pro:package_tree (master)


$ ruby init.rb`
```

## Setup with Docker
Additionally, a Docker file is included.
```
$ docker build -t package_tree .
$ docker run --rm -d -p 8080:8080 package_tree ruby /package_tree/init.rb

```
## Output of result
The output of automated test can be found at misceleanous file.

## Automated tests
Automated tests were made with `rspec` with about 16 test case.

├── spec
│   ├── package_control_spec.rb
│   ├── process_request_spec.rb
│   └── spec_helper.rb
└── test-capture
    └── test-screen.txt

```$ rspec spec/
................

Finished in 0.0154 seconds (files took 0.16334 seconds to load)
16 examples, 0 failures

Daels-MacBook-Pro:package_tree (master)
```
