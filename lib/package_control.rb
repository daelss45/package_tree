class PackageControl
  attr_accessor :package_dependencies

  def initialize
    @package_dependencies = {}
  end

  def index(package, dependencies=[])
    return add_package_dependencies(package, dependencies) if (!package_dependencies.key?(package) ||
    (package_dependencies.values.flatten & dependencies).sort.uniq == dependencies.sort.uniq ) ||
    package_dependencies.size == 0
    false
  end

  def remove(package, _=[])
    return remove_item(package) if !package_dependencies.values.flatten.include?(package)
    false
  end

  def query(item, _=[])
    return true if package_dependencies.key?(item) || package_dependencies.values.flatten.include?(item)
    false
  end

  private
  def add_package_dependencies(package, dependencies)
    package_dependencies[package] = dependencies
    true
  end

  def remove_item(item, _=[])
    package_dependencies.delete(item)
    true
  end
end
