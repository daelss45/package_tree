require_relative "package_control"

class Request

  VALID_COMMANDS = %w"INDEX REMOVE QUERY".freeze

  def initialize
    @package_instance ||= PackageControl.new
  end

  def valid_request?(input)
    VALID_COMMANDS.include?(input.split("|").first) &&
    !!(input =~ /(.+)\|(.+)\|(.*)/) &&
    input.count("|") == 2 &&
    input.split("|").last.include?("\n")
  end

  def message(result)
    result ? "OK\n" : "FAIL\n"
  end

  def get_command_package_dependencies(input)
    return "ERROR\n" if input.class == NilClass || !valid_request?(input)
    command, package, dependencies = input.chomp.split("|")
    dependencies = dependencies.class == NilClass ? [] : dependencies.split(",")
    dependencies
    result = @package_instance.send(command.downcase.to_sym, package, dependencies)
    message(result)
  end
end
